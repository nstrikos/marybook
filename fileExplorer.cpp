#include "fileExplorer.h"
#include <QDebug>

#include <QDir>

#if defined (Q_OS_ANDROID)
#include <QOperatingSystemVersion>
#include <QJniObject>
#include <QtCore/private/qandroidextras_p.h>
#endif

FileExplorer::FileExplorer(QObject *parent) : QObject(parent)
{
#if defined(Q_OS_ANDROID)
    accessAllFiles();
#endif
}

void FileExplorer::makeBackup(QString dir, QString filename)
{
    QString backDirString = dir + "backup";
    QString newFile = backDirString + "/" + filename;
    QString oldFile = dir + filename;

    QDir backDir(backDirString);
    QFile file(oldFile);
    if (!backDir.exists())
        backDir.mkpath(".");
    file.rename(newFile);
}

void FileExplorer::removeFile(QString filename)
{
    QFile::remove(filename);
}

void FileExplorer::rename(QString oldName, QString newName)
{
    QFile::rename(oldName, newName);
}

QString FileExplorer::read(QString filename)
{
    filename.replace("file://", "");
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString text = in.readAll();
        return text;
    }

    return "";
}

void FileExplorer::write(QString filename, QString text)
{
    qDebug() << filename;
    filename.replace("file://", "");
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "cannot write to file";
        return;
    }

    QTextStream out(&file);
    out << text;
}

bool FileExplorer::dirExists(QString directory)
{
    QString path = directory.replace("file://", "");
    QDir dir(path);
    return dir.exists();
}

#if defined(Q_OS_ANDROID)
void FileExplorer::accessAllFiles()
{
    if(QOperatingSystemVersion::current() < QOperatingSystemVersion(QOperatingSystemVersion::Android, 11)) {
        qDebug() << "it is less then Android 11 - ALL FILES permission isn't possible!";
        return;
    }
// Here you have to set your PackageName
#define PACKAGE_NAME "package:org.nstrikos.marybook"
    jboolean value = QJniObject::callStaticMethod<jboolean>("android/os/Environment", "isExternalStorageManager");
    if(value == false) {
        qDebug() << "requesting ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION";
        QJniObject ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION = QJniObject::getStaticObjectField( "android/provider/Settings", "ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION","Ljava/lang/String;" );
        QJniObject intent("android/content/Intent", "(Ljava/lang/String;)V", ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION.object());
        QJniObject jniPath = QJniObject::fromString(PACKAGE_NAME);
        QJniObject jniUri = QJniObject::callStaticObjectMethod("android/net/Uri", "parse", "(Ljava/lang/String;)Landroid/net/Uri;", jniPath.object<jstring>());
        QJniObject jniResult = intent.callObjectMethod("setData", "(Landroid/net/Uri;)Landroid/content/Intent;", jniUri.object<jobject>() );
        QtAndroidPrivate::startActivity(intent, 0);
    } else {
        qDebug() << "SUCCESS ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION";
    }
}
#endif
