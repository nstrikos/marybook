#include <QApplication>
#include <QQmlApplicationEngine>

#include <QQmlContext>

#include "fileExplorer.h"
#include "net/netClient.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    app.setOrganizationName("nstrikos");
    app.setApplicationName("Mary book");

    FileExplorer fileExplorer;

    QQmlApplicationEngine engine;

    NetClient netClient;

#ifdef Q_OS_ANDROID
    engine.rootContext()->setContextProperty("ANDROID_SUPPORT", QVariant(true));
#else
    engine.rootContext()->setContextProperty("ANDROID_SUPPORT", QVariant(false));
#endif // SMTP_SUPPORT


    engine.rootContext()->setContextProperty("fileExplorer", &fileExplorer);
    engine.rootContext()->setContextProperty("netClient", &netClient);

    const QUrl url(QStringLiteral("qrc:/newmain.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
