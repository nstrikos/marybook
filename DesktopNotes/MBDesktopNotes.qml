import QtQuick
import QtQuick.Window 2.2

import "noteDB.js" as NoteDB

Rectangle {
    id: root
    width: 800
    height: 600
    visible: true
    //visibility: Window.FullScreen
    //title: qsTr("Desktop")

    signal quit

    Rectangle  {
        id: window
        anchors.fill: parent
        color: "brown"

        property variant webfont: FontLoader {
            source: "qrc:/LiberationSans-Regular.ttf"
            onStatusChanged: {
                if (webfontloader.status === FontLoader.Ready)
                    console.log('Loaded')
            }
        }

        BorderImage {
            id: background
            anchors.fill: parent
            anchors.margins: -5
            source: "qrc:/images/background.png"
            border.left: 70; border.top: 77
            border.right: 90; border.bottom: 91
        }

        MarkerPanel {
            id: markerPanel
            width: 120
            anchors { right: window.right; top: window.top //bottom: window.bottom
                topMargin: 30
            }
        }

        PagePanel {
            id: pagePanel
            state: markerPanel.activeMarker
            anchors {
                right: markerPanel.left
                left: toolbar.right
                top: parent.top
                bottom: parent.bottom
                leftMargin: 1
                rightMargin: -50
                topMargin: 3
                bottomMargin: 15
            }
        }

        Rectangle {
            anchors.fill: toolbar
            color: "white"
            opacity: 0.15
            radius: 16
            border { color: "#600"; width: 4 }
        }

        Column {
            id: toolbar
            spacing: 16
            anchors {
                top: window.top; left: window.left; bottom: window.bottom;
                topMargin: 50; bottomMargin: 50; leftMargin: 8
            }

            Tool {
                id: quitTool
                source: "qrc:/images/close.png"
                onClicked: {
                    pagePanel.saveNotesToDB()
                    root.visible = false
                    quit()
                }
            }

            Tool {
                id: showListTool
                source: "qrc:/images/tool_pagelayout-light.png"
                onClicked: pagePanel.showList()
            }

            Tool {
                id: editTool
                source: "qrc:/images/edit.png"
                onClicked: {
                    pagePanel.canSave()
                    pagePanel.currentPage.editNotes()
                }
            }

            Tool {
                id: syncTool
                source: "qrc:/images/clear.png"
                onClicked: {
                    pagePanel.saveNotesToDB()
                    pagePanel.sendNotes()
                    netClient.sendMessage("sending notes ended")
                }
            }

            Tool {
                id: newNoteTool
                source: "qrc:/images/add.png"
                onClicked: {
                    pagePanel.canSave()
                    pagePanel.currentPage.newNote()
                }
            }
        }
    }

    function addNote(text) {
        pagePanel.addNote(text)
    }

    function saveNotes() {
        pagePanel.saveNotesToDB()
    }

    Component.onCompleted: {
        NoteDB.openDB()
    }
}
