import QtQuick

Item {
    id: root
    //width: 150; height: 450

    // a property of type string to hold the value of the current active marker
    property string activeMarker: "daily"

    // a list for holding respective data for a Marker item.
    property variant markerData: [
        { img: "qrc:/images/dailymarker.png", markerid: "daily" },
        { img: "qrc:/images/projectmarker.png", markerid: "project" },
        { img: "qrc:/images/variousmarker.png", markerid: "various" },
        { img: "qrc:/images/marymarker.png", markerid: "mary_virgin" }
    ]

    Column {
        id: layout
        anchors.fill: parent
        spacing: 0
        property int activeMarker

        scale: Screen.height > 500 ? 1.0 : 0.5

        Repeater {
            // using the defined list as our model
            model: markerData
            delegate:
                Marker {
                id: marker

                // the active property of the Marker is true only when the marker
                // item is the one current active one set on the onClicked signal
                active: layout.activeMarker == index

                source: modelData.img                 

                // handling the clicked signal of the Marker item, setting the currentMarker property
                // of MarkerPanel based on the clicked Marker
                onClicked: {
                    layout.activeMarker = index
                    root.activeMarker = modelData.markerid
                }
            }
        }
    }
}
