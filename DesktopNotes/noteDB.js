.pragma library

.import QtQuick.LocalStorage as Sql

var _db

function openDB() {
    print("noteDB.createDB()")
    _db = Sql.LocalStorage.openDatabaseSync("DesktopDB","1.0","The Desktop Database",1000000);
    createNoteTable();
}

function createNoteTable() {
    print("noteDB.createTable()")
    _db.transaction( function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS note (noteId INTEGER PRIMARY KEY AUTOINCREMENT, x INTEGER, y INTEGER, noteText TEXT, markerId TEXT)");
    });
}

function clearNoteTable() {
    print("noteDB.clearNoteTable()")
    _db.transaction( function(tx) {
        tx.executeSql("DELETE FROM note");
    });
}

function readNotesFromPage(markerId) {
    print("noteDB.readNotesFromPage() " + markerId)
    var noteItems = {}
    _db.readTransaction( function(tx) {
        var rs = tx.executeSql("SELECT * FROM note WHERE markerId=? ORDER BY markerid DESC", [markerId] );
        var item
        for (var i = 0; i < rs.rows.length; i++) {
            item = rs.rows.item(i)
            noteItems[item.noteId] = item;
        }
    });

    return noteItems;
}

function saveNotes(noteItems, markerId) {
    print("noteDB.saveNotes() for page: " + markerId +" notes.length: " + noteItems.length)
    for (var i = 0; i < noteItems.length; ++i) {
        var noteItem = noteItems[i]
        var text = noteItem.noteText.trim()
        if (text !== "") {
            _db.transaction( function(tx) {
                tx.executeSql("INSERT INTO note (markerId, x, y, noteText) VALUES(?,?,?,?)",[markerId, noteItem.x, noteItem.y, noteItem.noteText]);
            });
        }
    }
}
