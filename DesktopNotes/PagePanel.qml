import QtQuick

import "noteDB.js" as NoteDB

Item {
    id: root

    property Page currentPage: dailypage
    property bool save: false

    state: "daily"

    states: [
        State {
            name: "daily"
            PropertyChanges { target: dailypage; opacity:1.0; restoreEntryValues: true }
            PropertyChanges { target: root; currentPage: dailypage; explicit: true }
            PropertyChanges { target: dailypage; visible: true; restoreEntryValues: true }
            PropertyChanges { target: projectpage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: variouspage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: mary_virginpage; visible: false; restoreEntryValues: true }
        },

        State {
            name: "project"
            PropertyChanges { target: projectpage; opacity:1.0; restoreEntryValues: true }
            PropertyChanges { target: root; currentPage: projectpage;  explicit: true }
            PropertyChanges { target: projectpage; visible: true; restoreEntryValues: true }
            PropertyChanges { target: dailypage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: variouspage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: mary_virginpage; visible: false; restoreEntryValues: true }

        },
        State {
            name: "various"
            PropertyChanges { target: variouspage; opacity:1.0; restoreEntryValues: true }
            PropertyChanges { target: root; currentPage: variouspage; explicit: true }
            PropertyChanges { target: variouspage; visible: true; restoreEntryValues: true }
            PropertyChanges { target: dailypage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: projectpage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: mary_virginpage; visible: false; restoreEntryValues: true }
        },
        State {
            name: "mary_virgin"
            PropertyChanges { target: mary_virginpage; opacity:1.0; restoreEntryValues: true }
            PropertyChanges { target: root; currentPage: mary_virginpage; explicit: true }
            PropertyChanges { target: mary_virginpage; visible: true; restoreEntryValues: true }
            PropertyChanges { target: variouspage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: dailypage; visible: false; restoreEntryValues: true }
            PropertyChanges { target: projectpage; visible: false; restoreEntryValues: true }
        }
    ]

    transitions: [
        Transition {
            from: "*"; to: "*"
            NumberAnimation { property: "opacity"; duration: 500 }
        }
    ]

    BorderImage {
        id: background
        anchors.fill: parent
        source: "qrc:/images/page.png"
        border.left: 68; border.top: 69
        border.right: 40; border.bottom: 80
    }

    Page { id: dailypage; anchors.fill: parent; markerId: "daily"; caller: root }
    Page { id: projectpage; anchors.fill: parent; markerId: "project"; caller: root }
    Page { id: variouspage; anchors.fill: parent; markerId: "various"; caller: root }
    Page { id: mary_virginpage; anchors.fill: parent; markerId: "mary_virgin"; caller: root}

    function canSave() {
        save = true
    }

    //Component.onDestruction: saveNotesToDB()

    function saveNotesToDB() {
        if (save) {
            NoteDB.clearNoteTable();

            NoteDB.saveNotes(dailypage.notes, dailypage.markerId)
            NoteDB.saveNotes(projectpage.notes, projectpage.markerId)
            NoteDB.saveNotes(variouspage.notes, variouspage.markerId)
            NoteDB.saveNotes(mary_virginpage.notes, mary_virginpage.markerId)
        }
    }

    function addNote(text) {
        console.log(text)
        save = true
        var note
        if (text.startsWith("daily--")) {
            text = text.trim()
            note = text.substring(7)
            dailypage.addNote(note)
        }
        else if (text.startsWith("project--")) {
            text = text.trim()
            note = text.substring(9)
            projectpage.addNote(note)
        }
        else if (text.startsWith("various--")) {
            text = text.trim()
            note = text.substring(9)
            variouspage.addNote(note)
        }
        else if (text.startsWith("mary_virgin--")) {
            text = text.trim()
            note = text.substring(13)
            mary_virginpage.addNote(note)
        }
    }

    function sendNotes() {
        dailypage.sendNotes()
        projectpage.sendNotes()
        variouspage.sendNotes()
        mary_virginpage.sendNotes()
    }

    function showList() {
        dailypage.showList()
        projectpage.showList()
        variouspage.showList()
        mary_virginpage.showList()
    }
}
