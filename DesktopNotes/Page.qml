import QtQuick

import "noteDB.js" as NoteDB

Item {
    id: root
    // by default a page should not be visible,
    // page's visibility is managed by the PagePanel
    // the opacity property is sufficient
    opacity: 0.0

    // this property is held for helping to store the note
    // items in the database
    property string markerId

    // this property is used by the PagePanel componet
    // for retrieving all the notes of a page and storing
    // them in the Database.
    property alias notes: container.children
    property bool readOnly: true
    property bool listViewVisible: false

    property var caller

    // loading the Note Component
    Component {
        id: noteComponent
        Note { caller : root }
    }

    function aboutToDelete() {
        caller.canSave()
    }

    // creating an Item type that will be used as a note container
    // we anchor the container to fill the parent as it will be used
    // later in the code to control the dragging area for notes
    Item { id: container; anchors.fill: parent }

    // when the Component is loaded then the call the loadNotes() function
    // to load notes from the database
    Component.onCompleted: loadNotes()

    // a helper Javascript function that is reads the note data from DB
    function loadNotes() {
        var noteItems = NoteDB.readNotesFromPage(markerId)
        for (var i in noteItems) {
            var text = noteItems[i].noteText.trim()
            if (text !== "")
                newNoteObject(noteItems[i])
        }
    }

    // a Javascript helper function for creating Note QML objects
    function newNoteObject(args) {
        // setting the container property of the note to the
        // actual container see Note.qml what the container
        // property is used for
        args.container = container

        //if (text !== null)
        //    args.noteText = text

        // calling the createObject() function on the noteComponent
        // for creating Note objects.
        // the container will be the parent of the new object
        // and args as the set of arguments
        var note = noteComponent.createObject(container, args)
        if (note === null) {
            console.log("note object failed to be created!")
        }

        note.visible = !listViewVisible
        if (note.markerId === markerId)
            nameModel.append({ name: note.noteText })
    }

    // a Javascript helper function for iterating through
    // the children types of the container item
    // and calls destroy() for deleting them
    function clear() {
        for (var i = 0; i < container.children.length; ++i) {
            container.children[i].destroy()
        }
    }

    // this Javascript helper function is used to create,
    // not loaded from db, Note items so that it will
    // set the markerId property of the note.
    function newNote() {
        // calling the newNoteObject and passing the a set of arguments where the markerId is set.
        newNoteObject( { "markerId": root.markerId } )
    }

    function addNote(text) {
        addNoteObject( {"markerId": root.markerId}, text )
    }

    function addNoteObject(args, text) {
        // setting the container property of the note to the
        // actual container see Note.qml what the container
        // property is used for

        if (text === "" || noteExists(text))
            return;

        args.container = container

        args.noteText = text

        // calling the createObject() function on the noteComponent
        // for creating Note objects.
        // the container will be the parent of the new object
        // and args as the set of arguments
        var note = noteComponent.createObject(container, args)
        if (note === null) {
            console.log("note object failed to be created!")
        }
    }

    function noteExists(text) {
        var result = false;
        for (var i = 0; i < container.children.length; ++i) {
            if (text === (container.children[i].noteText)) {
                result = true;
                break;
            }

        }
        return result;
    }

    function sendNotes() {
        for (var i = 0; i < container.children.length; ++i) {
            var note = container.children[i].noteText;
            netClient.sendMessage(markerId + "--" + note)
        }
    }

    function editNotes() {
        root.readOnly = !root.readOnly
        for (var i = 0; i < container.children.length; ++i) {
            var note = container.children[i]
            note.readOnly = root.readOnly
        }
    }

    function disableNotes() {
        root.readOnly = true
        for (var i = 0; i < container.children.length; ++i) {
            var note = container.children[i]
            note.readOnly = true
        }
    }

    ListModel {
        id: nameModel
    }

    Component {
        id: nameDelegate
        Item {
            height: 100
            width: parent.width
            //anchors { left: parent.left; right: parent.right }
            //height: column.implicitHeight + 4
            Rectangle {
                anchors.fill: parent
                color: "white"
                border.width: 1
                border.color: "lightsteelblue"
                radius: 2
            }

            Text {
                text: name
                font.pixelSize: 24
            }
        }
    }

    ListView {
        id: listView
        visible: listViewVisible
        anchors.fill: parent
        anchors.topMargin: 50
        anchors.rightMargin: 50
        anchors.leftMargin: 50
        anchors.bottomMargin: 50
        clip: true
        model: nameModel
        delegate: nameDelegate
    }

    function showList() {
        nameModel.clear()
        listViewVisible = !listViewVisible

        for (var i = 0; i < container.children.length; ++i) {
            var note = container.children[i]
            if (note.markerId === markerId)
                nameModel.append({ name: note.noteText })
            note.visible = !listViewVisible
        }
    }
}

