import QtQuick
import QtQuick.Dialogs

Item {
    id: root
    width: 200; height: 200

    property string markerId
    property int noteId
    property alias noteText: editArea.text
    property bool readOnly: true

    property var caller

    // this property holds the container of notes
    // and is used to retrieve values for the maximum and minimum
    // dragging areas
    property variant container

    // setting the z order to 1 if the text area has the focus
    z: editArea.activeFocus ? 1:0

    BorderImage {
        id: noteImage
        anchors { fill: parent}
        border.left: 20; border.top: 20
        border.right: 20; border.bottom: 20

        Component.onCompleted: {
            if (markerId === "daily") source = "qrc:/images/daily_note.png"
            else if (markerId === "project") source = "qrc:/images/project_note.png";
            else if (markerId === "various") source = "qrc:/images/various_note.png";
            else source = "qrc:/images/mary_virgin_note.png";
        }
    }

    // creating a NoteToolbar item that will be anchored to its parent
    NoteToolBar {
        id: toolbar
        height: 40
        anchors { top: root.top; left: root.left; right: root.right }

        // using the drag property alias to set the drag.target to our Note item.
        drag.target: root

        // specifying the maximum and minimum dragging areas for the note
        drag.minimumX: root.container.x; drag.maximumX: root.container.width - root.width
        drag.minimumY: root.container.y; drag.maximumY: root.container.height - root.height

        // setting the focus on the text area when the toolbar is pressed
        onPressed: editArea.focus = true

        // creating the `delete` tool for deleting the note item
        Tool {
            id: deleteItem
            //anchors.right: parent.right
            //anchors.top: parent.top
            source: "qrc:/images/delete-note.png"
            onClicked: messageDialog.open()
        }
    }

    MessageDialog {
        id: messageDialog
        visible: false
        title: "This will delete the note"
        buttons: MessageDialog.Ok | MessageDialog.Close
        modality: Qt.ApplicationModal
        onAccepted: {
            aboutToDelete()
            root.destroy()
        }
    }


    // creating a TextEdit item
    TextEdit {
        id: editArea
        height: 150
        anchors { top: toolbar.bottom; left: parent.left; right: parent.right
            topMargin: 0; leftMargin: 20; rightMargin: 20; bottomMargin: 100
        }

        color: "blue"
        selectByMouse: true
        wrapMode: TextEdit.WrapAnywhere
        font.family: window.webfont.name
        font.pointSize: 13
        font.hintingPreference: Font.PreferFullHinting
        readOnly: root.readOnly

        // called when the painterHeight property changes
        // then the note height has to be updated based on the text input
        onPaintedHeightChanged: updateNoteHeight()
    }

    // defining a behavior when the height property changes for root type
    Behavior on height { NumberAnimation {} }

    // javascript helper function that calculates the height of the note
    // as more text input is entered or removed.
    function updateNoteHeight() {
        var noteMinHeight = 200
        var currentHeight = editArea.paintedHeight + toolbar.height +40;
        root.height = noteMinHeight

        if (currentHeight >= noteMinHeight)
            root.height = currentHeight
    }

    function aboutToDelete() {
        caller.aboutToDelete()
    }
}
