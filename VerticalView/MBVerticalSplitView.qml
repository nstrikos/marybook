import QtQuick 2.15
import QtQuick.Controls
import "./Page"
import "../FileExtensions.js" as FileExtensions

SplitView {
    id: verticalSplitView
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.left: fileBrowser.right
    anchors.right: parent.right
    orientation: Qt.Vertical

    signal newFileSelected
    signal newTextSelected
    signal mouseAreaPressed
    signal cornerMouse
    signal cornerMouseExited
    signal drawButtonPressed
    signal textBookExit
    signal textPressed
    signal fileButtonPressed

    MBPage {
        id: page
        implicitHeight: parent.height
        SplitView.preferredHeight: parent.height
    }

    Connections {
        target: page
        function onMouseAreaPressed() {
            mouseAreaPressed()
        }
        function onCornerMouse() {
            cornerMouse()
        }
        function onCornerMouseExited() {
            cornerMouseExited()
        }
    }

    MBTextBook {
        id: textBook
    }

    function fileSelected(source) {
        if (FileExtensions.extension(source) === ".txt") {
            newTextSelected()
            textBook.fileSelected(source)
        }
        else {
            newFileSelected()
            page.fileSelected(source)
        }
    }

    function updateFit() {
        page.updateFit()
    }

    MBStateMachine {
        id: stateMachine
    }

    function enterInitialState() {
        page.enterInitialState()
    }

    function enterShowImageState() {
        page.enterShowImageState()
    }

    function enterShowToolState() {
        page.enterShowToolState()
    }

    function exitShowToolState() {
        page.exitShowToolState()
    }

    function enterDrawState() {
        page.enterDrawState()
    }

    function enterTextState() {
        page.implicitHeight =  parent.height * 1 / 2
        page.SplitView.preferredHeight =  parent.height * 1 / 2
        textBook.implicitHeight =  parent.height * 1 / 2
        textBook.SplitView.preferredHeight = parent.height * 1 / 2
    }

    function exitTextState() {
        page.implicitHeight =  parent.height
        page.SplitView.preferredHeight =  parent.height
        textBook.implicitHeight =  0
        textBook.SplitView.preferredHeight = 0
    }

    function enterFileState() {
        page.enterFileState()
    }

    Connections {
        target: textBook
        function onTextBookExit() {
            textBookExit()
        }
    }
}
