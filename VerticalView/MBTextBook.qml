import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs
import "../FileExtensions.js" as FileExtensions

Rectangle {
    anchors.top: page.bottom
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right

    signal textBookExit

    property string txtFile: ""
    property string txtExt: ""
    property string txtDir: ""

    function fileSelected(source) {
        txtFile = FileExtensions.basename(source)
        txtDir = FileExtensions.directory(source)
        txtExt = FileExtensions.extension(source)
        textBook.openText(source)
    }

    function setText(text) {
        textEdit.text = text
    }

    function openText(source) {
        setText(fileExplorer.read(source))
    }

    FileDialog {
        id: saveFileDialog
        //selectExisting: false
        nameFilters: ["Text files (*.txt)", "All files (*)"]
        onAccepted: fileExplorer.write(selectedFile, textEdit.text)
        fileMode: FileDialog.SaveFile
    }

    Image {
        id: saveButton
        anchors.top: parent.top
        anchors.left: parent.left
        width: 50
        height: 50
        source: "qrc:/images/document-save.png"
        MouseArea {
            anchors.fill: parent
            onPressed: saveFileDialog.open()
        }
    }

    Image {
        id: decButton
        anchors.top: parent.top
        anchors.left: saveButton.right
        anchors.leftMargin: 25
        width: 50
        height: 50
        source: "qrc:/images/go-down.png"
        MouseArea {
            anchors.fill: parent
            onPressed: textEdit.font.pointSize--
        }
    }

    Image {
        id: incButton
        anchors.top: parent.top
        anchors.left: decButton.right
        anchors.leftMargin: 25
        width: 50
        height: 50
        source: "qrc:/images/go-up.png"
        MouseArea {
            anchors.fill: parent
            onPressed: textEdit.font.pointSize++
        }
    }

    Label {
        id: fileLabel
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        font.bold: true
        font.pixelSize: 22
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: txtFile
    }

    Image {
        id: closeButton
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.leftMargin: 25
        width: 50
        height: 50
        source: "qrc:/images/go-up.png"
        MouseArea {
            anchors.fill: parent
            onPressed: textBookExit()
        }
    }

    Image {
        id: deleteButton
        anchors.top: parent.top
        anchors.right: closeButton.left
        anchors.leftMargin: 25
        width: 50
        height: 50
        source: "qrc:/images/delete.png"

        MessageDialog {
            id: msgDialog
            property string filename : txtDir + txtFile + txtExt
            title: "Remove file: " + filename
            buttons: DialogButtonBox.Ok | DialogButtonBox.Cancel
            onAccepted: {
                fileExplorer.makeBackup(txtDir, txtFile + txtExt)
                visible = false
            }
            onRejected: visible = false
            visible: false
        }

        MouseArea {
            anchors.fill: parent
            onPressed: msgDialog.visible = true
        }
    }

    Flickable {
        anchors.top: saveButton.bottom
        anchors.topMargin: 25
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        boundsBehavior: Flickable.StopAtBounds
        contentHeight: textEdit.height;
        contentWidth: textEdit.width;
        clip: true
        TextArea.flickable: TextArea {
            id: textEdit
            Layout.fillWidth: true
        }
        Layout.fillWidth: true
        Layout.fillHeight: true
    }
}
