import QtQuick 2.15
import QtQml.StateMachine 1.0 as DSM

DSM.StateMachine {
    id: stateMachine
    initialState: showImageState
    running: true

    property bool drawStateActive: drawState.active
    property bool showToolStateActive: showToolState.active

    DSM.State {
        id: showImageState
        DSM.SignalTransition {
            targetState: showToolState
            signal: mouseAreaPressed
        }
        DSM.SignalTransition {
            targetState: showToolState
            signal: cornerMouse
        }
        DSM.SignalTransition {
            targetState: textState
            signal: newTextSelected
        }
        onEntered: {
            console.log("show image state")
            enterShowImageState()
        }
    }

    DSM.State {
        id: showToolState
        DSM.SignalTransition {
            targetState: showImageState
            signal: mouseAreaPressed
        }
        DSM.SignalTransition {
            targetState: showImageState
            signal: cornerMouseExited
        }
        DSM.SignalTransition {
            targetState: textState
            signal: newTextSelected
        }
        DSM.SignalTransition {
            targetState: textState
            signal: textPressed
        }
        DSM.SignalTransition {
            targetState: drawState
            signal: drawButtonPressed
        }
        DSM.SignalTransition {
            targetState: fileState
            signal: fileButtonPressed
        }
        onEntered: {
            console.log("showTool state")
            enterShowToolState()
        }
        onExited: {
            exitShowToolState()
        }
    }

    DSM.State {
        id: drawState
        DSM.SignalTransition {
            targetState: showImageState
            signal: drawButtonPressed
        }
        onEntered: {
            console.log("draw state")
            enterDrawState()
        }
    }

    DSM.State {
        id: textState
        DSM.SignalTransition {
            targetState: showImageState
            signal: textBookExit
        }
        DSM.SignalTransition {
            targetState: showImageState
            signal: newFileSelected
        }
        onEntered: {
            console.log("text state")
            enterTextState()
        }
        onExited: {
            exitTextState()
        }
    }

    DSM.State {
        id: fileState
        DSM.SignalTransition {
            targetState: showImageState
            signal: fileButtonPressed
        }
        DSM.SignalTransition {
            targetState: showImageState
            signal: newFileSelected
        }
        onEntered: {
            console.log("file state")
            enterFileState()
        }
    }
}
