import QtQuick 2.15

Canvas {
    id: canvas
    anchors {
        left: parent.left
        bottom: parent.bottom
    }
    width: image.width
    height: image.height
    scale: image.scale
    property real lastX
    property real lastY
    property color color: page.penColor

    onPaint: {
        if (stateMachine.drawStateActive) {
            var ctx = getContext('2d')
            ctx.lineWidth = page.penWidth
            ctx.strokeStyle = canvas.color
            ctx.beginPath()
            ctx.moveTo(lastX, lastY)
            lastX = canvasMouseArea.mouseX
            lastY = canvasMouseArea.mouseY
            ctx.lineTo(lastX, lastY)
            ctx.stroke()
        }
    }
}
