import QtQuick 2.15

Rectangle {
    id: nextRect
    anchors.right: parent.right
    anchors.verticalCenter: parent.verticalCenter
    height: 100
    width: 100
    color: "grey"
    opacity: 0.5
    visible: (!ANDROID_SUPPORT)

    Image {
        anchors.fill: parent
        source: "qrc:/images/left-arrow.png"
        transformOrigin: Item.Center
        rotation: 180
    }

    MouseArea {
        id: nextMouseArea
        anchors.fill: parent
        onPressed: {
            fileBrowser.next()
        }
    }
}
