import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

import "../../FileExtensions.js" as FileExtensions

Flickable {
    id: flickable

    //signals for state transition
    signal mouseAreaPressed

    boundsBehavior: Flickable.StopAtBounds
    contentHeight: container.height;
    contentWidth: container.width;
    clip: true

    enum FitZoom {
        FitWidth,
        FitScreen,
        ZoomFull,
        Custom
    }

    property real minZoom: 0.1;
    property real maxZoom: 4
    property real zoomStep: 0.05
    property bool canvasClear: true
    property string picFile: ""
    property string picExt: ""
    property string picDir: ""
    property alias canvas: canvas
    property alias canvasMouseArea: canvasMouseArea
    property int fitSelected: MBFlickable.FitZoom.FitWidth

    onWidthChanged: fit()
    onHeightChanged: fit()

    Item {
        id: container
        width: Math.max(image.width * image.scale, flickable.width)
        height: Math.max(image.height * image.scale, flickable.height)

        Image {
            id: image

            property real prevScale: 1.0;

            asynchronous: true
            cache: false
            smooth: true
            anchors.centerIn: parent
            fillMode: Image.PreserveAspectFit
            transformOrigin: Item.Center

            onScaleChanged: {
                if ((width * scale) > flickable.width) {
                    var xoff = (flickable.width / 2 + flickable.contentX) * scale / prevScale;
                    flickable.contentX = xoff - flickable.width / 2
                }
                if ((height * scale) > flickable.height) {
                    var yoff = (flickable.height / 2 + flickable.contentY) * scale / prevScale;
                    flickable.contentY = yoff - flickable.height / 2
                }
                prevScale = scale;
            }
            onStatusChanged: {
                if (status === Image.Ready) {
                    rotation = 0
                    flickable.fit()
                }
            }

            MBCanvas {
                id: canvas
            }

            PinchArea {
                anchors.fill: parent
                pinch.target: image
                pinch.minimumScale: 0.1
                pinch.maximumScale: 10

                onPinchFinished: flickable.fitSelected = MBFlickable.FitZoom.Custom

                MouseArea {
                    id: canvasMouseArea
                    preventStealing: stateMachine.drawStateActive ?  true : false
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton | Qt.MiddleButton

                    onPressed: (mouse) => {
                                   //if (stateMachine.showToolStateActive) {
                                       //mouseAreaPressed()
                                   //    mouse.accepted = false
                                   //}
                                   if (stateMachine.drawStateActive) {
                                       canvas.lastX = mouseX
                                       canvas.lastY = mouseY
                                       canvasClear = false
                                   }
                               }

                    onClicked: (mouse)=> {
                                   if (stateMachine.drawStateActive)
                                        return
                                   if (mouse.button === Qt.RightButton)
                                        zoomIn()
                                   else if (mouse.button === Qt.MiddleButton)
                                        zoomOut()
                               }

                    onPressAndHold: {
                        if (stateMachine.showToolStateActive) {
                            mouseAreaPressed()
                            //mouse.accepted = false
                        } else {
                            mouseAreaPressed()
                        }
                    }

                    onPositionChanged: {
                        if (stateMachine.drawStateActive)
                            canvas.requestPaint()
                    }
                }
            }
        }
    }

    // WheelHandler {
    //     onWheel: (event) => {
    //                  if (event.angleDelta.y > 0)
    //                  zoomIn()
    //                  else
    //                  zoomOut()
    //              }
    // }

    function fit() {
        if (fitSelected === MBFlickable.FitZoom.FitWidth)
            fitWidth()
        else if (fitSelected === MBFlickable.FitZoom.FitScreen)
            fitToScreen()
        else if (fitSelected === MBFlickable.FitZoom.ZoomFull)
            zoomFull()
    }

    function fitToScreen() {
        zoomFull()
        var s = Math.min(flickable.width / image.sourceSize.width, flickable.height / image.sourceSize.height)
        image.scale = s;
        flickable.minZoom = s;
        image.prevScale = scale
        contentY = 0
        fitSelected = MBFlickable.FitZoom.FitScreen
    }

    function fitWidth() {
        var s = flickable.width / image.sourceSize.width
        image.scale = s;
        canvas.scale = 1;
        flickable.minZoom = s;
        image.prevScale = scale
        flickable.returnToBounds();
        contentY = 0
        fitSelected = MBFlickable.FitZoom.FitWidth
    }

    function zoomIn() {
        image.scale *= (1.0 + zoomStep)
        fitSelected = MBFlickable.FitZoom.Custom
    }
    function zoomOut() {
        image.scale *= (1.0 - zoomStep)
        fitSelected = MBFlickable.FitZoom.Custom
    }
    function zoomFull() {
        console.log("zoom full")
        image.scale = 1;
        canvas.scale = 1;
        flickable.returnToBounds();
        contentY = 0
        fitSelected = MBFlickable.FitZoom.ZoomFull
    }

    function fileSelected(source) {
        var s = FileExtensions.extension(source)
        if (s === ".txt") {
            txtFile = FileExtensions.basename(source)
            txtDir = FileExtensions.directory(source)
            txtExt = FileExtensions.extension(source)
            textBook.openText(source)
        } else {
            picFile = FileExtensions.basename(source)
            picDir = FileExtensions.directory(source)
            picExt = FileExtensions.extension(source)
            updateImage(source)
        }
    }

    function updateImage(source) {
        var ctx = canvas.getContext('2d')
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasClear = true
        image.source = source
        flickable.contentY = 0
        picFile = FileExtensions.basename(source)
        picExt = FileExtensions.extension(source)
        picDir = FileExtensions.directory(source)
    }

    function updateFit() {
        fitSelected = MBFlickable.FitZoom.FitWidth
    }

    function rotateLeft() {
        image.rotation -= 0.1
    }

    function rotateRight() {
        image.rotation += 0.1
    }

    ScrollIndicator.vertical: ScrollIndicator { }
    ScrollIndicator.horizontal: ScrollIndicator { }

    function save() {
        var filename = picDir + picFile + "-edited.png"
        fileExplorer.makeBackup(picDir, picFile + picExt)
        var imscale = image.scale
        var canvasScale = canvas.scale
        image.scale = 1
        canvas.scale = 1
        messageDialog.visible = true
        container.grabToImage(function(result) {
            result.saveToFile(filename)
            image.scale = imscale
            canvas.scale = canvasScale
            messageDialog.visible = false;
        });

    }

    MessageDialog {
        id: messageDialog
        title: "Please wait..."
        text: "Please wait..."
        buttons: MessageDialog.NoButton
        visible: false
    }
}
