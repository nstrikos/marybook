import QtQuick 2.15

Rectangle {
    id: previousRect
    anchors.left: parent.left
    anchors.verticalCenter: parent.verticalCenter
    height: 100
    width: 100
    color: "grey"
    opacity: 0.5
    visible: (!ANDROID_SUPPORT)


    Image {
        anchors.fill: parent
        source: "qrc:/images/left-arrow.png"
    }

    MouseArea {
        id: previousMouseArea
        anchors.fill: parent
        onPressed: {
            fileBrowser.previous()
        }
    }
}
