import QtQuick 2.15

Rectangle {
    id: drawRect
    visible: true
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    height: 50
    width: 50
    color: "grey"
    opacity: 0.9
    radius: 20

    Image {
        anchors.fill: parent
        source: "qrc:/images/document-edit.png"
    }

    MouseArea {
        id: drawRectArea
        anchors.fill: parent
        onPressed: drawButtonPressed()
    }
}
