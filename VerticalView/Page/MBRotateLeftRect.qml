import QtQuick 2.15
import "../../SplitViewFunctions.js" as Functions

Rectangle {
    id: rotateLeftRect
    visible: true
    anchors.right: saveImageRect.left
    anchors.top: parent.top
    height: 50
    width: 50
    color: "grey"
    opacity: 0.5
    
    Image {
        id: transitionImage
        anchors.fill: parent
        source:  "qrc:/images/object-rotate-left.png"
    }
    
    MouseArea {
        anchors.fill: parent
        onPressed: rotateLeft()
    }
}
