import QtQuick 2.15

Rectangle {
    id: fullScreenRect
    visible: true
    anchors.right: parent.right
    anchors.top: parent.top
    height: 50
    width: 50
    color: "grey"
    opacity: 0.5

    Image {
        id: transitionImage
        anchors.fill: parent
        source:  { window.visibility == Window.FullScreen ?
                       "qrc:/images/file-zoom-out.png" :
                       "qrc:/images/file-zoom-in.png"
        }
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            if (window.visibility == Window.FullScreen) {
                window.showMaximized()
                transitionImage.source = "qrc:/images/file-zoom-in.png"
            } else if (window.visibility != Window.FullScreen) {
                window.showFullScreen()
                transitionImage.source = "qrc:/images/file-zoom-out.png"
            }
        }
    }
}
