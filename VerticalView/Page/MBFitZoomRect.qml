import QtQuick 2.15

Rectangle {
    id: fitZoomRect
    visible: true
    anchors.right: hideFileBrowserRect.left
    anchors.rightMargin: 25
    anchors.top: parent.top
    height: 50
    width: 50
    color: "grey"
    opacity: 0.5

    Image {
        anchors.fill: parent
        source: "qrc:/images/zoom-original.png"
    }

    MouseArea {
        id: fitZoomMouseArea
        anchors.fill: parent
        onPressed: {
            flickable.zoomFull()
        }
    }
}
