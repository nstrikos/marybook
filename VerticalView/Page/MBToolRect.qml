import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Dialogs

Rectangle {
    id: toolRect
    visible: true
    anchors.left: drawRect.right
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    height: 50
    color: "grey"
    opacity: 0.9
    radius: 20

    property int penWidth: 5

    Rectangle {
        id: blueButton
        width: 50
        height: 50
        anchors.left: parent.left
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        color: "blue"
        radius: 20

        MouseArea {
            anchors.fill: parent
            onPressed: {
                page.colorSelected("blue")
            }
        }
    }

    Rectangle {
        id: greenButton
        width: 50
        height: 50
        anchors.left: blueButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        color: "green"
        radius: 20

        MouseArea {
            anchors.fill: parent
            onPressed: {
                page.colorSelected("green")
            }
        }
    }

    Rectangle {
        id: yellowButton
        width: 50
        height: 50
        anchors.left: greenButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        color: "yellow"
        radius: 20

        MouseArea {
            anchors.fill: parent
            onPressed: {
                page.colorSelected("yellow")
            }
        }
    }

    Rectangle {
        id: redButton
        width: 50
        height: 50
        anchors.left: yellowButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        color: "red"
        radius: 20

        MouseArea {
            anchors.fill: parent
            onPressed: {
                page.colorSelected("red")
            }
        }
    }

    Image {
        id: colorButton
        width: 50
        height: 50
        anchors.left: redButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        source: "qrc:/images/color-management.png"

        MouseArea {
            anchors.fill: parent
            onPressed: colorDialog.visible = true
        }

        ColorDialog {
            id: colorDialog
            title: "Please choose a color"
            visible: false
            onAccepted: {
                page.colorSelected(colorDialog.selectedColor)
            }
        }
    }

    Button {
        id: decButton
        width: 50
        height: 50
        anchors.left: colorButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        text: "-"
        onClicked: {
            penWidth--
            if (penWidth < 1)
                penWidth = 1
        }
    }

    Button {
        id: incButton
        width: 50
        height: 50
        anchors.left: decButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        text: "+"
        onClicked: {
            penWidth++
            if (penWidth > 20)
                penWidth = 20
        }
    }

    Label {
        id: sizeLabel
        width: 25
        height: 25
        anchors.left: incButton.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 12
        text: penWidth
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Image {
        id: clearButton
        width: 50
        height: 50
        anchors.left: sizeLabel.right
        anchors.leftMargin: 11
        anchors.bottom: parent.bottom
        source: "qrc:/images/edit-reset.png"
        visible: !flickable.canvasClear

        MouseArea {
            anchors.fill: parent
            onPressed: {
                var ctx = flickable.canvas.getContext('2d')
                ctx.clearRect(0, 0, flickable.canvas.width, flickable.canvas.height);
                flickable.canvas.requestPaint()
                flickable.canvasClear = true
            }
        }
    }
}
