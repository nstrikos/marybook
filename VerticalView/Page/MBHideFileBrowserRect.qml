import QtQuick 2.15

Rectangle {
    id: hideFileBrowserRect
    visible: true
    anchors.right: fullScreenRect.left
    anchors.rightMargin: 25
    anchors.top: parent.top
    height: 50
    width: 50
    color: "grey"
    opacity: 0.5

    Image {
        id: fullScreenImage
        anchors.fill: parent
        source: "qrc:/images/auto-transition-right.png"
    }

    MouseArea {
        id: fullScreenMouseArea
        anchors.fill: parent
        onPressed: fileBrowser.hide()
    }
}
