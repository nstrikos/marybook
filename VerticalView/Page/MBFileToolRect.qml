import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Controls

Rectangle {
    id: fileToolRect
    visible: false
    anchors.left: parent.left
    anchors.right: fileRect.left
    anchors.bottom: parent.bottom
    height: 50
    color: "grey"
    opacity: 0.9
    radius: 20

    Image {
        id: fileRenameButton
        width: 50
        height: 50
        anchors.left: parent.left
        anchors.leftMargin: 100
        anchors.bottom: parent.bottom
        source: "qrc:/images/check_constraint.png"

        MouseArea {
            anchors.fill: parent
            onPressed: {
                dialog.visible = true
            }
        }
    }

    Image {
        id: fileRemoveButton
        width: 50
        height: 50
        anchors.left: fileRenameButton.right
        anchors.leftMargin: 25
        anchors.bottom: parent.bottom
        source: "qrc:/images/delete.png"

        MessageDialog {
            id: messageDialog
            property string filename : flickable.picDir + flickable.picFile + flickable.picExt
            title: "Remove file: " + filename
            buttons: DialogButtonBox.Ok | DialogButtonBox.Cancel
            onAccepted: {
                fileExplorer.makeBackup(flickable.picDir, flickable.picFile + flickable.picExt)
                visible = false
            }
            onRejected: visible = false
            visible: false
        }

        MouseArea {
            anchors.fill: parent
            onPressed: messageDialog.visible = true
        }
    }
}
