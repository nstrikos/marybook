import QtQuick 2.15

Rectangle {
    id: fitWidRect
    visible: true
    anchors.right: fitScreenRect.left
    anchors.rightMargin: 25
    anchors.top: parent.top
    height: 50
    width: 50
    color: "grey"
    opacity: 0.5

    Image {
        anchors.fill: parent
        source: "qrc:/images/resizecol.png"
    }

    MouseArea {
        id: zoomInMouseArea
        anchors.fill: parent
        onPressed: {
            flickable.fitWidth()
        }
    }
}
