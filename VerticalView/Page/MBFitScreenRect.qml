import QtQuick 2.15

Rectangle {
    id: fitScreenRect
    visible: true
    anchors.right: fitZoomRect.left
    anchors.rightMargin: 25
    anchors.top: parent.top
    height: 50
    width: 50
    color: "grey"
    opacity: 0.5

    Image {
        anchors.fill: parent
        source: "qrc:/images/zoom-fit-best.png"
    }

    MouseArea {
        id: fitScreenMouseArea
        anchors.fill: parent
        onPressed: {
            flickable.fitToScreen()
        }
    }
}
