import QtQuick 2.15

Rectangle {
    id: textRect
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    color: "grey"
    height: 50
    opacity: 0.5
    radius: 20
    visible: true
    width: 50

    Image {
        anchors.fill: parent
        source: "qrc:/images/tool_pagelayout.png"
    }

    MouseArea {
        id: textRectArea
        anchors.fill: parent
        onPressed: {
            textPressed()
        }
    }
}
