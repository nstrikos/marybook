import QtQuick 2.15
import QtQuick.Controls

Label {
    id: fileLabel
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter
    font.bold: true
    font.pixelSize: 22
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    text: flickable.picFile
}
