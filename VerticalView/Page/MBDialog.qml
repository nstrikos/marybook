import QtQuick 2.15
import QtQuick.Controls


Rectangle {
    id: dialog
    width: 500
    height: 90
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    visible: true

    TextField {
        id: textField
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 30
        text: "Current name: " + flickable.file
        readOnly: true
    }

    TextField {
        id: textInput
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: textField.bottom
        height: 30
    }

    Button {
        anchors.top: textInput.bottom
        anchors.right: parent.right
        height: 30
        text: "Ok"
        onClicked: {
            dialog.visible = false
            var oldName = flickable.dir + flickable.file + flickable.ext
            var newName = flickable.dir + textInput.text + flickable.ext
            fileExplorer.rename(oldName, newName)
        }
    }

    Button {
        text: "Cancel"
        anchors.top: textInput.bottom
        anchors.left: parent.left
        height: 30
        onClicked: {
            textInput.text = ""
            dialog.visible = false
        }
    }
}
