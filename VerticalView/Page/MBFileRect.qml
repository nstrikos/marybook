import QtQuick 2.15

Rectangle {
    id: fileRect
    visible: true
    anchors.right: textRect.left
    anchors.rightMargin: 25
    anchors.bottom: parent.bottom
    height: 50
    width: 50
    color: "grey"
    opacity: 0.9
    radius: 20

    Image {
        anchors.fill: parent
        source: "qrc:/images/folder-blue.png"
    }

    MouseArea {
        id: fileRectArea
        anchors.fill: parent
        onPressed: fileButtonPressed()
    }
}
