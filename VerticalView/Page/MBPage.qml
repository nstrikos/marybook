import QtQuick 2.15
import "../../SplitViewFunctions.js" as Functions

Item {
    id: page
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    height: parent.height

    property alias canvasMouseArea: flickable.canvasMouseArea
    property color penColor: "transparent"
    property alias penWidth: toolRect.penWidth

    signal mouseAreaPressed
    signal cornerMouse
    signal cornerMouseExited

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.5
    }

    Rectangle {
        id: rect1
        width: 100
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left

        color: "grey"
        opacity: 0.5
        visible: ANDROID_SUPPORT

        Image {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: 100
            source: "qrc:/images/left-arrow.png"
            fillMode: Image.Stretch
            transformOrigin: Item.Center
            clip: true
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                fileBrowser.previous()
            }
        }
    }

    Rectangle {
        id: rect2
        width: 100
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        visible: ANDROID_SUPPORT

        color: "grey"
        opacity: 0.5

        Image {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: 100
            source: "qrc:/images/left-arrow.png"
            fillMode: Image.Stretch
            transformOrigin: Item.Center
            rotation: 180
            clip: true
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                fileBrowser.next()
            }
        }
    }

    MBFlickable {
        id: flickable
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: rect1.right
        anchors.right: rect2.left

        Component.onCompleted: {
            if (ANDROID_SUPPORT) {
                anchors.top = parent.top
                anchors.bottom = parent.bottom
                anchors.left = rect1.right
                anchors.right = rect2.left
            } else {
                anchors.fill = parent
            }
        }
    }

    MouseArea {
        width: 100
        height: parent.height
        anchors.right: parent.right
        anchors.top: parent.top
        hoverEnabled: true
        enabled: !ANDROID_SUPPORT
        onEntered: {
            if (flickable.picFile != "")
                if (!ANDROID_SUPPORT)
                    cornerMouse()
        }
        onExited: { if (!ANDROID_SUPPORT)
                cornerMouseExited()
        }
    }

    MouseArea {
        enabled: !ANDROID_SUPPORT
        Timer {
            id: timer
            onTriggered: {
                if (flickable.picFile != "")
                    if (!ANDROID_SUPPORT)
                        cornerMouse()
            }
            interval: 500
            running: false
        }
        width: 100
        height: parent.height
        anchors.left: parent.left
        anchors.top: parent.top
        hoverEnabled: true
        onEntered: {
            timer.start()
        }
        onExited: {
            timer.stop()
            if (!ANDROID_SUPPORT)
                cornerMouseExited()
        }
    }

    MouseArea {
        enabled: !ANDROID_SUPPORT
        width: parent.width
        height: 100
        anchors.left: parent.left
        anchors.top: parent.top
        hoverEnabled: true
        onEntered: {
            if (flickable.picFile != "")
                if (!ANDROID_SUPPORT)
                    cornerMouse()
        }
        onExited: {
            if (!ANDROID_SUPPORT)
                cornerMouseExited()
        }
    }

    MouseArea {
        enabled: !ANDROID_SUPPORT
        width: parent.width
        height: 100
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        hoverEnabled: true
        onEntered: {
            if (flickable.picFile != "")
                if (!ANDROID_SUPPORT)
                    cornerMouse()
        }
        onExited: {
            if (!ANDROID_SUPPORT)
                cornerMouseExited()
        }
    }

    Connections {
        target: flickable
        function onMouseAreaPressed() {
            mouseAreaPressed()
        }
    }

    MBFullScreenRect {
        id: fullScreenRect
    }

    MBPreviousRect {
        id: previousRect
    }

    MBNextRect {
        id: nextRect
    }

    MBHideFileBrowserRect {
        id: hideFileBrowserRect
    }

    MBFitZoomRect {
        id: fitZoomRect
    }

    MBFitScreenRect {
        id: fitScreenRect
    }

    MBFitWidthRect {
        id: fitWidthRect
    }

    MBRotateLeftRect {
        id: rotateLeftRect
    }

    MBSaveImageRect {
        id: saveImageRect
    }

    MBRotateRightRect {
        id: rotateRightRect
    }

    MBFileLabel {
        id: fileLabel
    }

    MBDrawRect {
        id: drawRect
    }

    MBToolRect {
        id: toolRect
    }

    MBFileRect {
        id: fileRect
    }

    MBFileToolRect {
        id: fileToolRect
    }

    MBDialog {
        id: dialog
    }

    MBTextRect {
        id: textRect
    }

    function fileSelected(source) {
        flickable.fileSelected(source)
    }

    function updateFit() {
        flickable.updateFit()
    }

    function colorSelected(newColor) {
        penColor = newColor
        drawRect.border.color = penColor
    }

    function enterInitialState() {
        Functions.enterInitialState()
    }

    function enterShowImageState() {
        Functions.enterShowImageState()
    }

    function enterShowToolState() {
        Functions.enterShowToolState()
    }

    function exitShowToolState() {
        Functions.exitShowToolState()
    }

    function enterDrawState() {
        Functions.enterDrawState()
    }

    function enterFileState() {
        Functions.enterFileState()
    }

    function rotateLeft() {
        flickable.rotateLeft()
    }

    function rotateRight() {
        flickable.rotateRight()
    }

    function saveImage() {
        flickable.save()
    }
}
