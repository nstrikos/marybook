function enterInitialState() {
    fullScreenRect.visible = false
    previousRect.visible = false
    nextRect.visible = false
    fitZoomRect.visible = false
    fitScreenRect.visible = false
    hideFileBrowserRect.visible = false
    fitWidthRect.visible = false
    rotateLeftRect.visible = false
    rotateRightRect.visible = false
    saveImageRect.visible = false
    drawRect.visible = false
    toolRect.visible = false
    fileRect.visible = false
    fileLabel.visible = false
    fileToolRect.visible = false
    dialog.visible = false
    textRect.visible = false
    //page.canvasMouseArea.preventStealing = false
    //page.drawRect.border.color = "transparent"
    //page.drawRect.border.width = 0
    //page.fileRect.border.color = "transparent"
    //page.fileRect.border.width = 0
    //page.textRect.visible = false
    //page.textRect.border.color = "transparent"
    //page.textRect.border.width = 0
}

function enterShowImageState() {
    enterInitialState()
}

function enterShowToolState() {
    previousRect.visible = true
    nextRect.visible = true
    fullScreenRect.visible = true
    fitZoomRect.visible = true
    fitScreenRect.visible = true
    hideFileBrowserRect.visible = true
    fitWidthRect.visible = true
    rotateLeftRect.visible = true
    rotateRightRect.visible = true
    saveImageRect.visible = true
    drawRect.visible = true
    toolRect.visible = false
    fileRect.visible = true
    fileLabel.visible = true
    textRect.visible = true
    fileToolRect.visible = false
    page.canvasMouseArea.cursorShape = Qt.ArrowCursor
}

function exitShowToolState() {
    previousRect.visible = false
    nextRect.visible = false
    fullScreenRect.visible = false
    fitZoomRect.visible = false
    fitScreenRect.visible = false
    hideFileBrowserRect.visible = false
    fitWidthRect.visible = false
    rotateLeftRect.visible = false
    rotateRightRect.visible = false
    saveImageRect.visible = false
    drawRect.visible = false
    toolRect.visible = false
    fileRect.visible = false
    fileLabel.visible = false
    textRect.visible = false
    fileToolRect.visible = false
    //canvasMouseArea.cursorShape = Qt.ArrowCursor
}

function enterDrawState() {
    previousRect.visible = false
    nextRect.visible = false
    fullScreenRect.visible = false
    fitZoomRect.visible = false
    fitScreenRect.visible = false
    drawRect.visible = true
    toolRect.visible = true
    fileRect.visible = false
    fileLabel.visible = false
    hideFileBrowserRect.visible = false
    fitWidthRect.visible = false
    rotateLeftRect.visible = false
    rotateRightRect.visible = false
    saveImageRect.visible = false
    //canvasMouseArea.preventStealing = true
    //canvasMouseArea.cursorShape = Qt.PointingHandCursor
    drawRect.border.color = page.penColor
    drawRect.border.width = 8
    textRect.visible = false
}

function enterFileState() {
    previousRect.visible = false
    nextRect.visible = false
    fullScreenRect.visible = false
    fitZoomRect.visible = false
    fitScreenRect.visible = false
    rotateLeftRect.visible = false
    rotateRightRect.visible = false
    saveImageRect.visible = false
    drawRect.visible = false
    toolRect.visible = false
    fileRect.visible = true
    fileLabel.visible = true
    fileToolRect.visible = true
    //fileRect.border.color = window.color
    //fileRect.border.width = 8
}
