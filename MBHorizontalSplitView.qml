import QtQuick 2.15
import QtQuick.Controls
import "./FileBrowser"
import "./VerticalView"


SplitView {
    id: horizontalSplitView
    orientation: Qt.Horizontal

    signal quit()

    MBFileBrowser {
        id: fileBrowser
    }

    MBVerticalSplitView {
        id: verticalSplitView
    }

    Connections {
        target: fileBrowser
        function onFileSelected(source) {
            verticalSplitView.fileSelected(source)
        }
        function onUpdateFit() {
            verticalSplitView.updateFit()
        }
        function onQuit() {
            horizontalSplitView.visible = false
            horizontalSplitView.quit()
        }
    }
}
