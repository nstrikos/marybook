import QtQuick.Controls
import QtQuick

import "./DesktopNotes"

ApplicationWindow {
    id: window
    width: Screen.width
    height: Screen.height
    visible: true
    visibility: Window.FullScreen
    title: qsTr("Mary book")

    signal exit

    MBDesktopNotes {
        id: desktopNotes
        anchors.fill: parent
        visible: false
    }

    MBHorizontalSplitView {
        id: splitView
        anchors.fill: parent
        visible: false
    }

    Image {
        id: mainRect
        anchors.fill: parent
        source: "qrc:/images/desktop.jpg"

        Image {
            id: noteImg
            width: window.width / 5
            height: window.height / 2.5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: -width * 1.5
            source: "qrc:/images/daily_note.png"
            //opacity: 0.75
            scale: 0.90
            Behavior on scale {
                NumberAnimation  { duration: 200 }
            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    mainRect.visible = false
                    textEditor.visible = false
                    desktopNotes.visible = true
                    mainRect.enabled = false
                    textEditor.enabled = false
                    desktopNotes.enabled = true
                }
                hoverEnabled: true
                onEntered: noteImg.scale = 1.1
                onExited: noteImg.scale = 1.0
            }
        }

        Image {
            id: textImg
            width: window.width / 5
            height: window.height / 2.5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "qrc:/images/desktop-notes.png"
            //opacity: 0.75
            Behavior on scale {
                NumberAnimation  { duration: 200 }
            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    mainRect.visible = false
                    splitView.visible = false
                    textEditor.visible = true
                    mainRect.enabled = false
                    splitView.enabled = false
                    textEditor.enabled = true
                }
                hoverEnabled: true
                onEntered: textImg.scale = 1.1
                onExited: textImg.scale = 1.0
            }
        }

        Image {
            id: fileImg
            width: window.width / 5
            height: window.height / 2.5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: width * 1.5
            source: "qrc:/images/folder.png"
            //opacity: 0.75
            Behavior on scale {
                NumberAnimation  { duration: 200 }
            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    mainRect.visible = false
                    textEditor.visible = false
                    splitView.visible = true
                    mainRect.enabled = false
                    textEditor.enabled = false
                    splitView.enabled = true
                }
                hoverEnabled: true
                onEntered: fileImg.scale = 1.1
                onExited: fileImg.scale = 1.0
            }
        }

        Image {
            width: 50
            height: 50
            anchors.left: parent.left
            anchors.top: parent.top
            source: "qrc:/images/close.png"
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    Qt.quit()
                }
            }
        }
    }

    TextEditor {
        id: textEditor
        visible: false
    }

    Connections {
        target: desktopNotes
        function onQuit() {
            mainRect.visible = true
            mainRect.enabled = true
        }
    }

    Connections {
        target: splitView
        function onQuit() {
            mainRect.visible = true
            mainRect.enabled = true
        }
    }

    Connections {
        target: textEditor
        function onQuit() {
            mainRect.visible = true
            mainRect.enabled = true
        }
    }

    Connections {
        target: netClient
        function onNewMessage(from, message) {
            if (message === "sending notes ended") {
                desktopNotes.saveNotes()
            }
            else {
                desktopNotes.addNote(message)
            }
        }
    }

    onClosing: (close) => {
                   close.accepted = false
                   exit()
               }
}
