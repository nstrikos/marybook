import QtQuick.Controls
import QtQuick

ApplicationWindow {
    id: window
    width: Screen.width
    height: Screen.height
    visible: true
    visibility: Window.FullScreen
    title: qsTr("Mary book")

    signal exit

    MBHorizontalSplitView {
        anchors.fill: parent
    }

    onClosing: (close) => {
        close.accepted = false
        exit()
    }
}
