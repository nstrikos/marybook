import QtQuick 2.15
import QtQuick.Controls
import QtCore

Rectangle {
    id: titleBar
    color: "black"
    width: parent.width;
    height: itemHeight

    signal quit

    Image {
        id: exitButton
        height: titleBar.height
        width: 50
        source: "qrc:/images/blue-close.png"
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: scaledMargin
        MouseArea {
            anchors.fill: parent
            onPressed: quit()
        }
    }

    Image {
        id: homeButton
        height: titleBar.height
        width: 50
        source: "qrc:/images/folder-blue.png"
        anchors.left: exitButton.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: scaledMargin
        MouseArea {
            anchors.fill: parent
            onPressed: homeFolder()
        }
    }

    Rectangle {
        id: upButton
        width: titleBar.height
        height: titleBar.height
        color: "transparent"
        anchors.left: homeButton.right//parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: scaledMargin

        Image { anchors.fill: parent; anchors.margins: scaledMargin; source: "qrc:/images/icon_BackArrow.png" }
        MouseArea { id: upRegion; anchors.fill: parent; onClicked: up() }
        states: [
            State {
                name: "pressed"
                when: upRegion.pressed
                PropertyChanges { target: upButton; color: palette.highlight }
            }
        ]
    }

    Text {
        anchors.left: upButton.right; anchors.right: parent.right; height: parent.height
        anchors.leftMargin: 10; anchors.rightMargin: 4
        text: folderList.folder
        color: "white"
        elide: Text.ElideLeft; horizontalAlignment: Text.AlignLeft; verticalAlignment: Text.AlignVCenter
        font.pixelSize: fontSize
    }

    Rectangle {
        color: "#353535"
        width: parent.width
        height: 1
        anchors.top: titleBar.bottom
    }
}
