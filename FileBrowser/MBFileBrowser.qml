import QtQuick 2.15
import QtQuick.Controls
import Qt.labs.folderlistmodel 2.7
import QtCore

import "../FileExtensions.js" as FileExtensions

Rectangle {
    id: fileBrowser

    implicitWidth: parent.width * 1 / 4

    SplitView.preferredWidth: parent.width * 1 / 4
    SplitView.minimumWidth: parent.width * 1 / 4
    SplitView.maximumWidth: parent.width * 3 / 4

    property int pixDens: Math.ceil(Screen.pixelDensity)
    property int itemHeight: 10 * pixDens
    property int scaledMargin: 2 * pixDens
    property int fontSize:  Qt.platform.os === "android" ?  (2 * pixDens) : (5 * pixDens)

    property string folder
    property var count
    property string file

    Settings {
        id: settings
        property alias folder: fileBrowser.folder
    }

    signal fileSelected(string file)
    signal updateFit()
    signal quit()

    color: "transparent"

    function previous() {
        count--
        if (count < 0) count = 0
        var imageSource = loader.item.folders.get(count, "fileName")
        var s = FileExtensions.extension(imageSource)
        if (s === ".png" || s === ".jpg" || s === ".txt") {
            var path = loader.item.folders.folder + "/" + imageSource
            fileSelected(path)
        }
    }

    function next() {
        count++
        console.log(count, loader.item.folders.count)
        if (count === loader.item.folders.count) count = 0
        var imageSource = loader.item.folders.get(count, "fileName")
        var s = FileExtensions.extension(imageSource)
        if (s === ".png" || s === ".jpg" || s === ".txt") {
            var path = loader.item.folders.folder + "/" + imageSource
            fileSelected(path)
        }
    }

    function hide() {
        if (visible === false) {
            width = 400
            visible = true
        } else {
            minimumWidth = 0
            width = 0
            visible = false
        }
    }

    Component.onCompleted: {
        fileBrowser.folder = settings.folder

        if (fileBrowser.folder == "" || (!fileExplorer.dirExists(fileBrowser.folder)) )
            fileBrowser.folder = StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileBrowser.show()
    }

    function show() {
        loader.sourceComponent = fileBrowserComponent
        loader.item.parent = fileBrowser
        loader.item.anchors.fill = fileBrowser
        loader.item.count = fileBrowser.count
        loader.item.folder = fileBrowser.folder
    }

    function selectFile(file, counter) {
        if (file !== "") {
            folder = loader.item.folderList.folder
            fileBrowser.fileSelected(file)
            count = counter
            fileBrowser.file = file
        }
    }

    Loader {
        id: loader
    }

    SystemPalette {
        id: palette
    }

    Component {
        id: fileBrowserComponent

        Rectangle {
            id: root
            color: "black"
            property variant folderList: foldersList
            property alias folder: foldersList.folder
            property variant folders: foldersList
            property variant count: folders.count

            function up() {
                var path = folderList.parentFolder;
                if (path.toString().length === 0 || path.toString() === 'file:')
                    return;
                foldersList.folder = path;
            }

            function down(path) {
                foldersList.folder = path;
            }

            function homeFolder() {
                foldersList.folder = StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
            }

            FolderListModel {
                id: foldersList
                folder: folder
                showDirsFirst: true

                onFolderChanged: settings.folder = folder
            }

            ListView {
                id: view
                anchors.top: titleBar.bottom
                anchors.bottom: parent.bottom
                width: parent.width
                model: foldersList
                delegate: folderDelegate
            }

            Component {
                id: folderDelegate

                Rectangle {
                    id: wrapper
                    function launch() {
                        var path = "file://";
                        if (filePath.length > 2 && filePath[1] === ':') // Windows drive logic, see QUrl::fromLocalFile()
                            path += '/';
                        path += filePath;
                        if (folderList.isFolder(index)) {
                            down(path);
                        }
                        else {
                            fileBrowser.updateFit()
                            fileBrowser.selectFile(path, folders.indexOf(path))
                        }
                    }
                    width: root.width
                    height: itemHeight
                    color: "transparent"

                    Item {
                        width: itemHeight; height: itemHeight
                        Image {
                            source: folderList.isFolder(index) ? "qrc:/images/icon_Folder.png" : "qrc:/images/image.png"
                            fillMode: Image.PreserveAspectFit
                            anchors.fill: parent
                            anchors.margins: scaledMargin
                        }
                    }

                    Text {
                        id: nameText
                        anchors.fill: parent; verticalAlignment: Text.AlignVCenter
                        text: fileName
                        anchors.leftMargin: itemHeight + scaledMargin
                        font.pixelSize: fontSize
                        color: "white"
                        elide: Text.ElideRight
                    }

                    MouseArea {
                        id: mouseRegion
                        anchors.fill: parent
                        onClicked: {
                            wrapper.ListView.view.currentIndex = index
                            if (folderList === wrapper.ListView.view.model)
                                launch()
                        }
                    }
                }
            }

            MBFileBrowserTitleBar {
                id: titleBar
            }

            Connections {
                target: titleBar
                function onQuit() {
                    fileBrowser.quit()
                }
            }
        }
    }



}
