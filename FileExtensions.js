function extension(str)
{
    var s = (str.slice(str.lastIndexOf("/")+1))
    var f = s.indexOf(".")
    return s.slice(f)
}

function basename(str)
{
    var s = (str.slice(str.lastIndexOf("/")+1))
    var f = s.indexOf(".")
    return s.substring(0, f)
}


function directory(str) {
    var s = str.replace("file://", "")
    var f = s.lastIndexOf("/") + 1
    return s.substring(0, f)
}
