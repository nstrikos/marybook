import QtQuick.Controls
import QtQuick
import QtCore
import QtQuick.Window
import QtQuick.Dialogs

Rectangle {
    //id: window
    id: textEditor
    width: 1024
    height: 600
    visible: true
    anchors.fill: parent
    color: "white"
    //title: textArea.textDocument.source +
    //       " - Text Editor Example" + (textArea.textDocument.modified ? " *" : "")

    signal quit

    Component.onCompleted: {
        x = Screen.width / 2 - width / 2
        y = Screen.height / 2 - height / 2
    }

    Action {
        id: quitAction
        onTriggered: {
            aboutToClose()
        }
    }
    Action {
        id: openAction
        onTriggered: {
            if (textArea.textDocument.modified)
                discardDialog.open()
            else
                openDialog.open()
        }
    }
    Action {
        id: saveAction
        enabled: textArea.textDocument.modified
        onTriggered: textArea.textDocument.save()
    }
    Action {
        id: saveAsAction
        onTriggered: saveDialog.open()
    }
    Action {
        id: copyAction
        enabled: textArea.selectedText
        onTriggered: textArea.copy()
    }
    Action {
        id: cutAction
        enabled: textArea.selectedText
        onTriggered: textArea.cut()
    }
    Action {
        id: pasteAction
        enabled: textArea.canPaste
        onTriggered: textArea.paste()
    }
    Action {
        id: undoAction
        onTriggered: textArea.undo()
    }
    Action {
        id: redoAction
        onTriggered: textArea.redo()
    }
    Action {
        id: boldAction
        checkable: true
        checked: textArea.cursorSelection.font.bold
        onTriggered: textArea.cursorSelection.font = Qt.font({ bold: checked, pixelSize: textArea.cursorSelection.font.pixelSize, underline: textArea.cursorSelection.font.underline, italic: textArea.cursorSelection.font.italic })
    }
    Action {
        id: italicAction
        checkable: true
        checked: textArea.cursorSelection.font.italic
        onTriggered: textArea.cursorSelection.font = Qt.font({ italic: checked, pixelSize: textArea.cursorSelection.font.pixelSize, underline: textArea.cursorSelection.font.underline, bold: textArea.cursorSelection.font.bold })
    }
    Action {
        id: underlineAction
        checkable: true
        checked: textArea.cursorSelection.font.underline
        onTriggered: textArea.cursorSelection.font = Qt.font({ underline: checked, pixelSize: textArea.cursorSelection.font.pixelSize, bold: textArea.cursorSelection.font.bold, italic: textArea.cursorSelection.font.italic })
    }
    Action {
        id: alignLeftAction
        checkable: true
        checked: textArea.cursorSelection.alignment === Qt.AlignLeft
        onTriggered: textArea.cursorSelection.alignment = Qt.AlignLeft
    }
    Action {
        id: alignCenterAction
        checkable: true
        checked: textArea.cursorSelection.alignment === Qt.AlignCenter
        onTriggered: textArea.cursorSelection.alignment = Qt.AlignCenter
    }
    Action {
        id: alignRightAction
        checkable: true
        checked: textArea.cursorSelection.alignment === Qt.AlignRight
        onTriggered: textArea.cursorSelection.alignment = Qt.AlignRight
    }
    Action {
        id: alignJustifyAction
        checkable: true
        checked: textArea.cursorSelection.alignment === Qt.AlignJustify
        onTriggered: textArea.cursorSelection.alignment = Qt.AlignJustify
    }
    
    FileDialog {
        id: openDialog
        fileMode: FileDialog.OpenFile
        selectedNameFilter.index: 1
        nameFilters: ["Text files (*.txt)", "HTML files (*.html *.htm)", "Markdown files (*.md *.markdown)"]
        currentFolder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        onAccepted: {
            textArea.textDocument.modified = false // we asked earlier, if necessary
            textArea.textDocument.source = selectedFile
        }
    }
    
    FileDialog {
        id: saveDialog
        fileMode: FileDialog.SaveFile
        selectedNameFilter.index: 1
        nameFilters: openDialog.nameFilters
        currentFolder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        onAccepted: textArea.textDocument.saveAs(selectedFile)
    }
    
    FontDialog {
        id: fontDialog
        onAccepted: textArea.cursorSelection.font = selectedFont
    }
    
    ColorDialog {
        id: colorDialog
        selectedColor: "black"
        onAccepted: textArea.cursorSelection.color = selectedColor
    }
    
    MessageDialog {
        title: qsTr("Error")
        id: errorDialog
    }
    
    MessageDialog {
        id : quitDialog
        title: qsTr("Quit?")
        text: qsTr("The file has been modified. Quit anyway?")
        buttons: MessageDialog.Yes | MessageDialog.No
        onButtonClicked: function (button, role) {
            if (role === MessageDialog.YesRole) {
                textArea.textDocument.modified = false
                textEditor.visible = false
                quit()
            }
        }
    }
    
    MessageDialog {
        id : discardDialog
        title: qsTr("Discard changes?")
        text: qsTr("The file has been modified. Open a new file anyway?")
        buttons: MessageDialog.Yes | MessageDialog.No
        onButtonClicked: function (button, role) {
            if (role === MessageDialog.YesRole)
                openDialog.open()
        }
    }
    
    Rectangle {
        id: header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 42
        width: 50
        color: "light blue"

        Flow {
            id: flow
            width: parent.width

            Button {
                id: quitButton
                action: quitAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/close.png"
                }
            }
            Button {
                id: openButton
                action: openAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/document-open-50.png"
                }
            }
            Button {
                id: saveButton
                action: saveAction
                width:42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/document-save.png"
                }
            }
            Button {
                id: saveAsButton
                action: saveAsAction
                width:42
                height: 42
                flat: true
                ToolTip.text: "Save the active project"
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/document-save-as-50.png"
                }
            }
            Button {
                id: copyButton
                action: copyAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/edit-copy-50.png"
                }
            }
            Button {
                id: cutButton
                action: cutAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/edit-cut-50.png"
                }
            }
            Button {
                id: pasteButton
                action: pasteAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/edit-paste-50.png"
                }
            }
            Button {
                id: undoButton
                action: undoAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/edit-undo-50.png"
                }
            }
            Button {
                id: redoButton
                action: redoAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/edit-redo-50.png"
                }
            }
            Button {
                id: boldButton
                action: boldAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/format-text-bold-50.png"
                }
            }
            Button {
                id: italicButton
                action: italicAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/format-text-italic-50.png"
                }
            }
            Button {
                id: underlineButton
                action: underlineAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/format-text-underline-50.png"
                }
            }
            Button {
                id: fontFamilyToolButton
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/gtk-select-font-50.png"
                }
                onClicked: function () {
                    fontDialog.selectedFont = textArea.cursorSelection.font
                    fontDialog.open()
                }
            }
            Button {
                id: textColorButton
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/input-keyboard-color-50.png"
                }
                onClicked: function () {
                    colorDialog.selectedColor = textArea.cursorSelection.color
                    colorDialog.open()
                }
            }
            Button {
                id: alignLeftButton
                action: alignLeftAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/align-horizontal-left-50.png"
                }
            }
            Button {
                id: alignCenterButton
                action: alignCenterAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/align-vertical-center-50.png"
                }
            }
            Button {
                id: alignRightButton
                action: alignRightAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/align-horizontal-right-50.png"
                }
            }
            Button {
                id: alignJustifyButton
                action: alignJustifyAction
                width: 42
                height: 42
                flat: true
                Image {
                    anchors.fill: parent
                    source: "qrc:/images/gnumeric-format-valign-justify-50.png"
                }
            }
        }
    }
    
    Flickable {
        id: flickable
        flickableDirection: Flickable.VerticalFlick
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        
        ScrollBar.vertical: ScrollBar {}
        
        TextArea.flickable: TextArea {
            id: textArea
            textFormat: Qt.AutoText
            wrapMode: TextArea.Wrap
            focus: true
            selectByMouse: true
            persistentSelection: true
            leftPadding: 6
            rightPadding: 6
            topPadding: 0
            bottomPadding: 0
            background: null
            
            onLinkActivated: function (link) {
                Qt.openUrlExternally(link)
            }

            TapHandler {
                acceptedButtons: Qt.RightButton
                onTapped: {
                    if (!ANDROID_SUPPORT)
                        contextMenu.open()
                }
            }

            Menu {
                id: contextMenu

                MenuItem {
                    text: qsTr("Copy")
                    enabled: copyAction.enabled
                    onTriggered: copyAction.trigger()
                }
                MenuItem {
                    text: qsTr("Cut")
                    enabled: cutAction.enabled
                    onTriggered: cutAction.trigger()
                }
                MenuItem {
                    text: qsTr("Paste")
                    enabled: pasteAction.enabled
                    onTriggered: pasteAction.trigger()
                }

                MenuSeparator {}

                MenuItem {
                    text: qsTr("Font...")
                    onTriggered: function () {
                        fontDialog.selectedFont = textArea.cursorSelection.font
                        fontDialog.open()
                    }
                }

                MenuItem {
                    text: qsTr("Color...")
                    onTriggered: function () {
                        colorDialog.selectedColor = textArea.cursorSelection.color
                        colorDialog.open()
                    }
                }
            }

            // Component.onCompleted: {
            //     if (Qt.application.arguments.length === 2)
            //         textDocument.source = "file:" + Qt.application.arguments[1]
            //     else
            //         textDocument.source = "qrc:/texteditor.html"
            // }

            textDocument.onStatusChanged: {
                // a message lookup table using computed properties:
                // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer
                const statusMessages = {
                    [ TextDocument.ReadError ]: qsTr("Failed to load “%1”"),
                    [ TextDocument.WriteError ]: qsTr("Failed to save “%1”"),
                    [ TextDocument.NonLocalFileError ]: qsTr("Not a local file: “%1”"),
                }
                const err = statusMessages[textDocument.status]
                if (err) {
                    errorDialog.text = err.arg(textDocument.source)
                    errorDialog.open()
                }
            }
        }
    }

    function aboutToClose() {
        if (textArea.textDocument.modified) {
            quitDialog.open()
        } else {
            textEditor.visible = false
            quit()
        }
    }
}
